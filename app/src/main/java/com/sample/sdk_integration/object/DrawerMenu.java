package com.sample.sdk_integration.object;

public class DrawerMenu {



	public static final int HOME = 0;
	public static final int FLASH_FEATURES = -1;
	public static final int PHOTO_EDITOR = 6;
	public static final int TRANSLATION = 18;
	public static int FLASH_TYPE =-1;
	public static final int UPGRADE_TO_PREMIUM = 9999999;

	private int type;
	private String title;
	private int iconResource;


	public DrawerMenu(){
		super();
	}


	public DrawerMenu(int type, String title){
		this.type = type;
		this.title = title;
	}


	public DrawerMenu(int type, String title, int iconResource){
		this.type = type;
		this.title = title;
		this.iconResource = iconResource;
	}


	public int getType() {
		return type;
	}


	public void setType(int type) {
		this.type = type;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public int getIconResource() {
		return iconResource;
	}


	public void setIconResource(int iconResource) {
		this.iconResource = iconResource;
	}



}
