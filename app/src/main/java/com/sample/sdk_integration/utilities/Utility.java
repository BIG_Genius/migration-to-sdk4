package com.sample.sdk_integration.utilities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.view.Window;

import com.sample.sdk_integration.R;

import org.json.JSONObject;


public class Utility {


    // Check internet connection
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        return (cm == null || cm.getActiveNetworkInfo() == null) ? false : cm
                .getActiveNetworkInfo().isConnectedOrConnecting();
    }

    public static void showAlertDialog(final Context context, final String message) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setMessage(message);
        dialogBuilder.setPositiveButton(R.string.ok, null);
        dialogBuilder.create().show();

    }

    public static Dialog createDialog(Context mcontext) {

        Dialog dialog;
        dialog = new Dialog(mcontext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.view_progress);
        dialog.setCancelable(false);

        return dialog;
    }

    public static void removeDialog(Dialog dialog) {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception e) {

        }
    }


    public static void setDeeplinkUser(Context context, final boolean value){
        // Save boolean value on preference
        PreferenceData.setBooleanPrefs(PreferenceData.KEY_IS_DEEPLINK_USER,context,value);
    }
    public static boolean isDeeplinkUser(Context context){
        // read boolean value from preference
        // you can use this function for check, is user if from deeplink or not.
        //true : Deeplink user
        //false : Organic user
        return PreferenceData.getBooleanPrefs(PreferenceData.KEY_IS_DEEPLINK_USER,context);

    }

    public static boolean getFireAD(final JSONObject deeplinkObj){
        try {

            if (deeplinkObj != null && deeplinkObj.has("firead") && deeplinkObj.get("firead").toString().equalsIgnoreCase("false")) {
                return false;
            }

            return true;

        }catch (Exception e){
            return true;
        }
    }

}
